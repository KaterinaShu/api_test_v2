import {DefaultCrudRepository, juggler} from '@loopback/repository';
import {AccessToken} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class AccessTokenRepository extends DefaultCrudRepository<
  AccessToken,
  typeof AccessToken.prototype.id
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(AccessToken, dataSource);
  }
}
