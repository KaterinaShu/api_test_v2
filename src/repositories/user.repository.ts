import {DefaultCrudRepository, juggler, Filter, Where} from '@loopback/repository';

import {User, UserRegistration} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UserRepository extends DefaultCrudRepository<User,
  typeof User.prototype.id> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(User, dataSource);
  }

  async isUnic(newUser: UserRegistration): Promise<boolean> {

    const byMail = await this.findOne({where: {email: newUser.email}});
    const byUsername = await this.findOne({where: {username: newUser.username}});

    return !(byMail || byUsername);
  }
}
