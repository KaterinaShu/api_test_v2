import {Entity, model, property} from '@loopback/repository';

@model()
export class AccessToken extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  exp: number;

  @property({
    type: 'date',
    required: true,
  })
  created: Date;

  @property({
    type: 'string',
    required: true,
  })
  token: string;

  @property({
    type: 'number',
    required: true,
  })
  userId: number;

  constructor(data?: Partial<AccessToken>) {
    super(data);
  }
}
