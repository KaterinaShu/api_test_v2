import {Model, model, property} from '@loopback/repository';

@model()
export class UserRegistration extends Model {
  @property({
    type: 'string',
    required: true,
  })
  username: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
  })
  password?: string;

  constructor(data?: Partial<UserRegistration>) {
    super(data);
  }
}
