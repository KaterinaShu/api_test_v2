import {Model, model, property} from '@loopback/repository';

@model()
export class UserLogin extends Model {
  @property({
    type: 'string',
  })
  username?: string;

  @property({
    type: 'string',
  })
  password?: string;

  constructor(data?: Partial<UserLogin>) {
    super(data);
  }
}
