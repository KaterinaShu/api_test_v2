import {repository} from '@loopback/repository';
import {post, requestBody, HttpErrors, param} from '@loopback/rest';
import {hash, compare} from 'bcryptjs';
import {sign, decode} from 'jsonwebtoken';

import {User, UserLogin, UserRegistration} from '../models';
import {UserRepository, AccessTokenRepository} from '../repositories';
import * as isemail from 'isemail';
import request = require("superagent");

export class AuthorizationController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(AccessTokenRepository)
    public accessTokenRepository: AccessTokenRepository,
  ) {
  }

  /**
   * User registration endpoint
   * @param user: UserRegistration
   */
  @post('/registration', {
    responses: {
      '200': {
        description: 'UserRegistration model instance',
        content: {'application/json': {'x-ts-type': UserRegistration}},
      },
    },
  })
  async registration(@requestBody() user: UserRegistration): Promise<User> {

    // Validate Email
    if (!isemail.validate(user.email)) {
      throw new HttpErrors.UnprocessableEntity('invalid email');
    }

    // Check is user unique
    if (!await this.userRepository.isUnic(user)) {
      throw new HttpErrors.UnprocessableEntity('User already exists');
    }

    //Hash password
    if (user.password != null) {
      user.password = await hash(user.password, 10);
    }

    return await this.userRepository.create(user);
  }

  /**
   * User registration endpoint
   * @param user: UserLogin
   */
  @post('/login', {
    responses: {
      '200': {
        description: 'UserLogin model instance',
        content: {'application/json': {'x-ts-type': UserRegistration}},
      },
    },
  })
  async login(@requestBody() user: UserLogin): Promise<{'token': string}>{

    if( !user || !user.username || !user.password){
      throw new HttpErrors.BadRequest('Required field is empty');
    }
    const existingUser = await this.userRepository.findOne({ where: {username: user.username}});

    if (!existingUser){
      throw new HttpErrors.Forbidden('Invalid credentials');
    }

    if( await compare(user.password,existingUser.password)){
      throw new HttpErrors.Forbidden('Invalid credentials');
    }

    const playload = {
      userid: existingUser.id,
      created: Date.now(),
      exp: Math.floor(Date.now() / 1000) + (60 * 60),
    };

    const jwt = await sign(
      playload
      , 'very secret' );

    await this.accessTokenRepository.create(
      {
        userId: playload.userid,
        created: playload.created,
        exp: playload.exp,
        token: jwt
      });

    return {token: jwt};
  }

  /**
   * User registration endpoint
   * @param id
   * @param token
   */
  @post('/logout', {
    responses: {
      '200': {
        description: 'Logout model instance',
        content: {'application/json': {'x-ts-type': UserRegistration}},
      },
    },
  })
  async logout( @param.header.string('Authorization') token: string){

    token = token.replace('Bearer ', '');
    const tokenInDb = await this.accessTokenRepository.findOne({where:{token: token}});

    if ( tokenInDb ){
      await this.accessTokenRepository.delete(tokenInDb);
      return {
        message: 'Success',
      }
    }

    return {
      message: 'Already logged out',
    };
  }
}
