CREATE TABLE public."user"
(
    username text,
    password text NOT NULL,
    email text NOT NULL,
    id integer DEFAULT nextval('user_id_seq'::regclass) PRIMARY KEY NOT NULL
);