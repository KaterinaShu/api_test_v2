CREATE TABLE public.accesstoken
(
    id integer DEFAULT nextval('accesstoken_id_seq'::regclass) PRIMARY KEY NOT NULL,
    exp integer DEFAULT 1209600,
    token text,
    created timestamp with time zone,
    userid integer
);